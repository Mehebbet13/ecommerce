export const COLORS = Object.freeze({
    background: "#1E1F28",
    primary: "#EF3651",
    white: "#F7F7F7",
    dark: "#2A2C36",
    gray: "#ABB4BD",
    error: "#FF2424",
    success: "#55D85A",
    sale: "#FF3E3E",
    ordinaryText: "#F5F5F5",

});